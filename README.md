# reactDemo

## 2-5 服务端加载
#### 为什么需要服务端加载
> 1.seo 不友好，蜘蛛程序无法抓取网站内容。

> 2.首屏加载时间长，因为js代码需要先下载到本地然后在执行，这样多了一个执行的步骤。

#### 实现原理
> 先把代码打包好，然后用 node 服务读取成字符串，然后用 node http 模块返回到前端

#### 修改打包方法
> 1. 添加 webpack.config.server.js 文件。 打服务端渲染的包（nodejs 渲染用）
```
  需要注意的：
  target: 'node', 告诉 webpakc 打包后的执行环境
  output: {
    ...
    libraryTarget: 'commonjs2' 什么方式打包
  }
```

#### 使用 template.html 定义打包模板
> 在模板中定义 id="root" 节点，渲染时替换节点中的内容

#### 创建 nodejs 后端服务，实现服务端渲染
> 引入 express 和 react-dom/server 来完成渲染
> 1. 读取编译后的 dist/index.html
> 2. 创建静态资源托管 app.use('/public', 静态资源的 dist 目录)
> 3. 在 server 中读取 ReactSSR.renderToString(), res.send() 到 client
---

## 2-4 babel 编译 jxs react
#### react-dom 把 react 组建挂在到 dom 节点中
#### babel 编译 react 需要引入的包 和 基本配置
> babel 配置 创建 .babelrc
```
  {
    "presets": [
      ["es2015", {"looes": true}],
      "react"
    ]
  }
```

> 需要引入的包
> npm i -d

> babel-loader (webpack 配置)

> babel-core (babel 核心代码)

> babel-preset-es2015 (编译 es6 )

> babel-preset-es2015-loose (编译 es6，编译是否严禁)

> babel-preset-react （编译 react 代码）

#### html-webpack-plugin
> webpack 编译时的 html 模板，可以吧打包好的静态资源文件自动 按照 output 的路径注入到模板中。而且 html-webpack-plugin 的定制能力很强

> 最简单的用法
```
  plugins: [
    new HTMLWebpackPlguin()
  ]
```

---
## 2-3 webpack
#### webpack 的核心就是 loader
> css img js 等静态资源文件 以及 react 的 jsx 都有对应的 loading , 使用 loader 来打包编译这些文件

#### webpack 的基本应用
> 配置 webpack 时必须要有 entry  和  output

#### 基本配置

```
const path = require('path');

module.exports = {
  entry: {
    app: path.join(__dirname, '../client/app.js')
  },
  output: {
    filename: '[name].[hash].js',
    path: path.join(__dirname, '../dist'),
    publicPath: './public'
  }
}

```

---
## 2-2 web 常用的网络优化
#### 优化方法
> 1.合并资源文件, 减少 http 请求
```
  pc 的 http 并发请求有8个,手机更少
```
> 2.压缩代码
> 3.合理使用缓存, 尽可能减少请求

---
## 2-1 Web App 架构简介

#### 工程架构需要考虑都需要考虑那些
>解放生产力
```
  目光聚焦在业务代码上,其他的都尽可能的自动化.
  比如 自动化部署, 验证, 等

  源码预处理
  自动打包,自动更新页面显示
  自动处理图片依赖,保证开发和正式环境的统一
  静态资源的版本管理
```
> 围绕解决方案搭建环境, 其实就是搭建脚手架
```
  例子: react : react + redux + wabpack + es6
  预期可能出现的问题, 其实就是框架是否容易扩展.
```
> 定制代码规范
```
  1. 方便团队协作, 沟通
  2. 方便排错

  code lint
  不同开发环境的差异
  git commit 预处理
```

#### 项目架构
```
  需要考虑的事情
    网页如何运行
    业务代码如何分层,更好的实现功能. 增强项目的扩展能力
  技术选型
    react vue 等
  数据解决方案
    redux mobulex API 请求,
  整体代码风格
    那些是要存在 store 中, 那些要存在 当前页面中
```
